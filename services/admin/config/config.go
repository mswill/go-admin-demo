package config

import (
	"fmt"
	"github.com/spf13/viper"
)

func init() {
	fmt.Println("config init: ")
	viper.Set("SERVER_PORT", "10000")
}
