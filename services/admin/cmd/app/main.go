package main

import (
	log "github.com/sirupsen/logrus"
	_ "gitlab.com/mswill/go-admin-demo/config"
	"gitlab.com/mswill/go-admin-demo/infrastructure/server"
	"gitlab.com/mswill/go-admin-demo/internal/delivery/http"
	repoPostgres "gitlab.com/mswill/go-admin-demo/internal/reposiroty/postgres"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/logic"
	"gitlab.com/mswill/go-admin-demo/pkg/store/postgres"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// get instance
	app := server.GetServerInstance()

	// connect to db
	db := postgres.ConnectBD()
	//fmt.Println(db)

	pgxDB := postgres.ConnectPGX()

	// dependencies
	repo := repoPostgres.NewRepositories(db, pgxDB)
	usecase := logic.NewServices(repo)
	handler := http.NewHandlers(usecase)

	handler.PublicRoutes(app)

	//
	log.Printf("%s\n ", "this is logrus")
	osChan := make(chan os.Signal, 1)
	signal.Notify(osChan, syscall.SIGTERM, syscall.SIGINT)

	go server.Run(app)

	// listen an interrupt
	<-osChan
	server.ShutDown(app)
}
