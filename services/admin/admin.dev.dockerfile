FROM golang:1.19-alpine

RUN echo "----------------- ADMIN SERVICE -----------------"

#deploy
RUN mkdir /app
WORKDIR /app

RUN apk add --update make git vim curl gcc
RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b $(go env GOPATH)/bin
RUN echo "\n--------------- AIR ---------------"

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
#CMD ["go", "run", "/app/cmd/app/main.go"]
CMD ["air"]
