package http

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"math"
	"net/http"
	"strconv"
)

func (h *Handlers) GetProducts(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "products"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//-
	// pagination
	page, _ := strconv.Atoi(ctx.Query("page", "1"))
	limit := 10
	offset := (page - 1) * limit

	products, countProducts, err := h.prodSrvs.GetProducts(ctx.Context(), offset)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/products/meta": fiber.Map{
			"total":     countProducts,
			"page":      page,
			"last_page": math.Ceil(float64(countProducts/limit) + 1),
		},
		"/api/products": products,
	})
}

func (h *Handlers) CreateProduct(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "products"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//1
	var prod domain.Product

	//-
	err := ctx.BodyParser(&prod)
	if err != nil {
		return err
	}

	if prod.Title == "" || prod.Description == "" || prod.Image == "" || prod.Price == 0 {
		return ctx.JSON(fiber.Map{
			"/api/products/": "all fields should be filled",
		})
	}

	createProduct, err := h.prodSrvs.CreateProduct(ctx.Context(), &prod)
	// bad
	ctx.Status(http.StatusBadRequest)
	if err != nil {
		return ctx.JSON(fiber.Map{
			"/api/products/msg": err.Error(),
		})
	}

	// ok
	ctx.Status(http.StatusOK)
	return ctx.JSON(fiber.Map{
		"/api/user": createProduct,
	})

}

func (h *Handlers) GetProductByID(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "products"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))

	byID, err := h.prodSrvs.GetProductByID(ctx.Context(), id)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/products/:id": byID,
	})
}

func (h *Handlers) UpdateProduct(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "products"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))
	var prod domain.Product

	err := ctx.BodyParser(&prod)
	if err != nil {
		return err
	}

	updatedProd, err := h.prodSrvs.UpdateProductByID(ctx.Context(), &prod, id)
	if err != nil {
		return err
	}
	fmt.Println(prod)
	return ctx.JSON(fiber.Map{
		"/api/product/:id": updatedProd,
	})
}

func (h *Handlers) DeleteProduct(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "products"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))

	retProd, err := h.prodSrvs.DeleteProductByID(ctx.Context(), id)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/product/:id": retProd,
	})
}
