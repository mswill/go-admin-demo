package http

import (
	"github.com/gofiber/fiber/v2"
)

func (h *PerHandler) AllPermissions(ctx *fiber.Ctx) error {

	permissions, err := h.perSrvs.AllPermissions(ctx.Context())
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/permissions": permissions,
	})
}
