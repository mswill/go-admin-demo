package http

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/reposiroty/DTO"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func (h *Handlers) UpdateInfo(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	userID := ctx.UserContext().Value("userID")
	id, _ := strconv.Atoi(userID.(string))
	var uDTO DTO.UpdateUserInfo
	//-
	err := ctx.BodyParser(&uDTO)
	if err != nil {
		fmt.Println("could to parse user DTO")
		return err
	}

	u := &domain.User{
		ID:        uint(id),
		FirstName: uDTO.FirstName,
		LastName:  uDTO.LastName,
		Email:     uDTO.Email,
	}

	//-
	updatedUser, err := h.authSrvs.UpdateInfo(ctx.Context(), u)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/users/info": updatedUser,
	})
}

func (h *Handlers) UpdatePassword(ctx *fiber.Ctx) error {
	//if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
	//	return ctx.JSON(fiber.Map{
	//		"permissions": err.Error(),
	//	})
	//}
	//
	userID := ctx.UserContext().Value("userID")
	id, _ := strconv.Atoi(userID.(string))

	//-
	var uP DTO.UpdateUserPassword
	err := ctx.BodyParser(&uP)

	fmt.Println("FOR UPDATE PASSWORD: ", uP)
	//-
	if uP.Password != uP.ConfirmPassword {
		return ctx.JSON(fiber.Map{
			"/api/users/password": "passwords should be equal!",
		})
	}

	//-
	user := &domain.User{
		ID:       uint(id),
		Password: uP.Password,
	}
	if err != nil {
		fmt.Println("could not parse")
		return err
	}

	//-
	updatedUserPassword, err := h.authSrvs.UpdatePassword(ctx.Context(), user, uP.Password)
	if err != nil {
		return err
	}

	//-
	return ctx.JSON(fiber.Map{
		"/api/users/password": updatedUserPassword,
	})
}

func (h *AuthHandler) Register(ctx *fiber.Ctx) error {
	var u domain.User

	err := ctx.BodyParser(&u)
	if err != nil {
		return err
	}

	if u.FirstName == "" || u.LastName == "" || u.Email == "" || u.Password == "" {
		return ctx.JSON(fiber.Map{
			"/api/register": "all fields must be filled",
		})
	}

	u.Password = strings.TrimSpace(u.Password)
	u.RoleID = 1
	fmt.Println(u)

	// service
	res, err := h.authSrvs.SignUp(ctx.Context(), &u)

	// bad
	ctx.Status(fiber.StatusBadRequest)
	if err != nil {
		return ctx.JSON(fiber.Map{
			"msg": err.Error(),
		})

	}

	// ok
	ctx.Status(fiber.StatusOK)
	return ctx.JSON(fiber.Map{
		"msg": res,
	})
}

func (h *AuthHandler) Login(ctx *fiber.Ctx) error {

	//-
	var u domain.User
	err := ctx.BodyParser(&u)
	if err != nil {
		return err
	}

	res, err := h.authSrvs.SignIn(ctx.Context(), &u)

	//-
	// no found
	if err == usecase.ErrRecordNotFound {
		ctx.Status(http.StatusBadRequest)
		ctx.Set("Content-Type", "application/json;charset=utf-8")
		return ctx.JSON(fiber.Map{
			"msg": "incorrect data",
		})
	}

	//-
	// error
	if err != nil {
		return err
	}

	//-
	// ok
	// header
	ctx.Status(http.StatusOK) // set to header status
	ctx.Set("Token", res)     // set to header Token

	//-
	// cookie
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    res,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	} // create cookie and TTL
	ctx.Cookie(&cookie) // set cookie

	//-
	_ = ctx.JSON(fiber.Map{
		"token": res,
	})
	return nil
}

func (h *AuthHandler) Logout(ctx *fiber.Ctx) error {

	c := &fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	ctx.Cookie(c)

	return ctx.JSON(fiber.Map{
		"api/logout": "you are logout!",
	})
}

func (h *AuthHandler) GetUser(ctx *fiber.Ctx) error {

	userID := ctx.UserContext().Value("userID")
	user, err := h.authSrvs.GetUser(ctx.Context(), userID.(string))

	if err != nil {
		return err
	}

	ctx.Status(fiber.StatusOK)
	ctx.JSON(fiber.Map{
		"/api/getUser": user,
	})

	return nil
}

// test route
func (h *AuthHandler) GetTest(ctx *fiber.Ctx) error {

	ctx.JSON(fiber.Map{
		"/GetTest": "is ok!",
	})

	return nil
}
