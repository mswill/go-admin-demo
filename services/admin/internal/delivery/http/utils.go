package http

import (
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/mswill/go-admin-demo/internal/reposiroty/postgres"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"strconv"
)

//func GenerateJWT(user *domain.User) (string, error) {
//	claims := &postgres.MyCustomClaims{
//		ID:        int(user.ID),
//		FirstName: user.FirstName,
//		LastName:  user.LastName,
//		Email:     user.Email,
//		RegisteredClaims: jwt.RegisteredClaims{
//			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
//			IssuedAt:  jwt.NewNumericDate(time.Now()),
//		},
//	}
//
//	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
//	signedString, err := token.SignedString(postgres.MySigningKey)
//	if err != nil {
//		return "", err
//	}
//
//	return signedString, nil
//}

func ParseJWT(cookie string) (string, error) {
	//fmt.Println("ParseJWT: cookies: ", cookie)
	//
	token, err := jwt.ParseWithClaims(cookie, &postgres.MyCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return postgres.MySigningKey, nil
	})

	if err != nil || !token.Valid {
		return "", usecase.ErrAuthenticated
	}
	claims := token.Claims.(*postgres.MyCustomClaims)

	return strconv.Itoa(claims.ID), nil
}
