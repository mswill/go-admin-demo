package http

import (
	"encoding/csv"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"math"
	"os"
	"strconv"
)

func (h *Handlers) CreateOrder(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "orders"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	var o domain.Order
	//-
	err := ctx.BodyParser(&o)
	if err != nil {
		return err
	}

	//-
	order, err := h.orderSrvs.CreateOrder(ctx.Context(), &o)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/order": order,
	})
}

func (h *Handlers) GetOrders(ctx *fiber.Ctx) error {

	//-
	page, _ := strconv.Atoi(ctx.Query("page", "1"))
	limit := 5 // count for page content result
	offset := (page - 1) * limit
	//-
	orders, countOrders, err := h.orderSrvs.GetOrders(ctx.Context(), offset)
	if err != nil {
		return err
	}

	//-
	//----

	return ctx.JSON(fiber.Map{
		"/api/orders/meta": fiber.Map{
			"total":     countOrders,
			"page":      page,
			"last_page": math.Ceil(float64(countOrders / limit)),
		},
		"/api/orders": orders,
	})
}

func (h *Handlers) ExportCSV(ctx *fiber.Ctx) error {
	fPath := "./csv/orders.csv"
	fmt.Println()

	//-
	orders, _, err := h.orderSrvs.GetOrders(ctx.Context(), math.MaxInt)
	if err != nil {
		return err
	}

	//-
	err = CreateFIle(fPath, orders)
	if err != nil {
		return err
	}

	return ctx.Download(fPath)
}
func CreateFIle(filePath string, ords *[]domain.Order) error {
	fmt.Println()

	//-
	file, err := os.Create(filePath)
	defer file.Close()
	if err != nil {
		fmt.Println("File Err")
		return err
	}

	//-
	writer := csv.NewWriter(file)
	defer writer.Flush()

	//-
	err = writer.Write([]string{"ID", "Name", "Email", "Product Title", "Price", "Quantity"})
	if err != nil {
		return err
	}

	for _, o := range *ords {
		data := []string{
			strconv.Itoa(int(o.ID)),
			o.FirstName + "" + o.LastName,
			o.Email,
			"",
			"",
			"",
		}
		err := writer.Write(data)
		if err != nil {
			fmt.Println("write err")
			return err
		}

		for _, ots := range o.OrderItems {
			data := []string{
				"",
				"",
				"",
				ots.ProductTitle,
				strconv.Itoa(int(ots.Price)),
				strconv.Itoa(int(ots.Quantity)),
			}
			err := writer.Write(data)
			if err != nil {
				fmt.Println("write err")
				return err
			}
		}
	}

	return nil
}

func (h *Handlers) GetOrdersCharts(ctx *fiber.Ctx) error {
	fmt.Println()
	charts, err := h.orderSrvs.GetOrdersCharts(ctx.Context())
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"api/order/charts": charts,
	})
}
