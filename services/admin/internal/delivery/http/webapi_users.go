package http

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"math"
	"net/http"
	"strconv"
)

// done
func (h *Handlers) CreateUser(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	var user domain.User

	err := ctx.BodyParser(&user)
	if err != nil {
		return err
	}

	createUser, err := h.userSrvs.CreateUser(ctx.Context(), &user)
	// bad
	ctx.Status(http.StatusBadRequest)
	if err != nil {
		return ctx.JSON(fiber.Map{
			"/api/users/msg": err.Error(),
		})
	}

	// ok
	ctx.Status(http.StatusOK)
	return ctx.JSON(fiber.Map{
		"/api/users": createUser,
	})

}

// done
func (h *Handlers) GetUsers(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//-
	// pagination
	page, _ := strconv.Atoi(ctx.Query("page", "1"))
	limit := 10 // count for page content result
	offset := (page - 1) * limit

	users, countUsers, err := h.userSrvs.GetUsers(ctx.Context(), offset)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/users/meta": fiber.Map{
			"total":     countUsers,
			"page":      page,
			"last_page": math.Ceil(float64(countUsers/limit) + 1),
		},
		"/api/users": users,
	})
}

// done
func (h *Handlers) GetUserByID(ctx *fiber.Ctx) error {
	id, _ := strconv.Atoi(ctx.Params("id"))

	byID, err := h.userSrvs.GetUserByID(ctx.Context(), id)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/user/:id": byID,
	})
}

// done
func (h *Handlers) UpdateUser(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	id, _ := strconv.Atoi(ctx.Params("id"))
	var u domain.User

	err := ctx.BodyParser(&u)
	if err != nil {
		return err
	}

	user, err := h.userSrvs.UpdateUser(ctx.Context(), &u, id)
	if err != nil {
		return err
	}
	return ctx.JSON(fiber.Map{
		"/api/users/:id": user,
	})
}

// done
func (h *Handlers) DeleteUser(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "users"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}

	id, _ := strconv.Atoi(ctx.Params("id"))

	res, err := h.userSrvs.DeleteUser(ctx.Context(), id)
	if err != nil {
		return err
	}
	return ctx.JSON(fiber.Map{
		"/api/users": res,
	})
}
