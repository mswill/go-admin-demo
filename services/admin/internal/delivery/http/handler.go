package http

import (
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

type AuthHandler struct {
	authSrvs usecase.IAuthService
}
type UserHandler struct {
	userSrvs usecase.IUserService
}
type RoleHandler struct {
	roleSrvs usecase.IRoleService
}
type PerHandler struct {
	perSrvs usecase.IPermissionService
}
type ProdHandler struct {
	prodSrvs usecase.IProductService
}
type OrderHandler struct {
	orderSrvs usecase.IOrderService
}
type IsAuthorizedHandler struct {
	isAuthorizedSrvs usecase.IsAuthorizedMiddlewareService
}
type Handlers struct {
	AuthHandler
	UserHandler
	RoleHandler
	PerHandler
	ProdHandler
	OrderHandler
	IsAuthorizedHandler
}

func NewHandlers(srvs usecase.IServices) *Handlers {
	return &Handlers{
		AuthHandler:         AuthHandler{authSrvs: srvs},
		UserHandler:         UserHandler{userSrvs: srvs},
		RoleHandler:         RoleHandler{roleSrvs: srvs},
		PerHandler:          PerHandler{perSrvs: srvs},
		ProdHandler:         ProdHandler{prodSrvs: srvs},
		OrderHandler:        OrderHandler{orderSrvs: srvs},
		IsAuthorizedHandler: IsAuthorizedHandler{isAuthorizedSrvs: srvs},
	}
}
