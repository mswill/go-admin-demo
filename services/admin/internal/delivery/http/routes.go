package http

import "github.com/gofiber/fiber/v2"

func (h *Handlers) PublicRoutes(app *fiber.App) {
	app.Get("/test", h.GetTest)

	// static server
	app.Static("/api/uploads", "./uploads")

	// auth service
	app.Post("/api/register", h.Register)
	app.Post("/api/login", h.Login)

	// middleware need authenticate
	app.Use(isAuthenticated)

	app.Post("/api/users/info", h.UpdateInfo)
	app.Post("/api/users/password", h.UpdatePassword)

	app.Get("/api/user", h.GetUser)
	app.Post("/api/logout", h.Logout)

	// user service
	app.Get("/api/users", h.GetUsers)
	app.Post("/api/users", h.CreateUser)
	app.Get("/api/users/:id", h.GetUserByID)
	app.Put("/api/users/:id", h.UpdateUser)
	app.Delete("/api/users/:id", h.DeleteUser)

	// roles service
	app.Get("/api/roles", h.GetRoles)
	app.Post("/api/roles", h.CreateRole)
	app.Get("/api/roles/:id", h.GetRoleByID)
	app.Put("/api/roles/:id", h.UpdateRole)
	app.Delete("/api/roles/:id", h.DeleteRole)

	// permissions service
	app.Get("/api/permissions", h.AllPermissions)

	// products service
	app.Get("/api/products", h.GetProducts)
	app.Post("/api/products", h.CreateProduct)
	app.Get("/api/products/:id", h.GetProductByID)
	app.Put("/api/products/:id", h.UpdateProduct)
	app.Delete("/api/products/:id", h.DeleteProduct)

	// image upload
	app.Post("/api/upload", Upload)

	// order route
	app.Post("/api/orders", h.CreateOrder)
	app.Get("/api/orders", h.GetOrders)
	app.Get("/api/orders/charts", h.GetOrdersCharts)
	app.Post("/api/orders/export", h.ExportCSV)
}
