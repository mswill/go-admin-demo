package http

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/reposiroty/DTO"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"net/http"
	"strconv"
)

func (h *Handlers) GetRoles(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "roles"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	roles, err := h.roleSrvs.GetRoles(ctx.Context())
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/roles": roles,
	})
}

func (h *Handlers) CreateRole(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "roles"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//

	var roleDTO DTO.RoleCreateDTO
	//
	err := ctx.BodyParser(&roleDTO)
	if err != nil {
		return err
	}

	// create permissions slice
	permissions := make([]domain.Permissions, len(roleDTO.Permissions))

	for i, perID := range roleDTO.Permissions {
		id, _ := strconv.Atoi(perID)
		permissions[i] = domain.Permissions{
			ID: uint(id),
		}
	}

	role := domain.Role{
		RoleName:    roleDTO.RoleName,
		Permissions: permissions,
	}
	//
	createRole, err := h.roleSrvs.CreateRole(ctx.Context(), &role)
	// bad
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return ctx.JSON(fiber.Map{
			"/api/role/msg": err.Error(),
		})
	}

	// ok
	ctx.Status(http.StatusOK)
	return ctx.JSON(fiber.Map{
		"/api/role/": createRole,
	})

}

func (h *Handlers) GetRoleByID(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "roles"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))

	byID, err := h.roleSrvs.GetRoleByID(ctx.Context(), id)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/role/:id": byID,
	})
}

func (h *Handlers) UpdateRole(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "roles"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))
	var roleDTO DTO.RoleCreateDTO

	err := ctx.BodyParser(&roleDTO)
	if err != nil {
		return err
	}
	fmt.Println("input data: ", roleDTO)
	//-
	permissions := make([]domain.Permissions, len(roleDTO.Permissions))

	for i, perID := range roleDTO.Permissions {
		id, _ := strconv.Atoi(perID)
		permissions[i] = domain.Permissions{
			ID: uint(id),
		}
	}

	role := domain.Role{
		RoleName:    roleDTO.RoleName,
		Permissions: permissions,
	}
	fmt.Println("UPDATE ", role)
	updateRole, err := h.roleSrvs.UpdateRole(ctx.Context(), &role, id)
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/roles/:id": updateRole,
	})
}

func (h *Handlers) DeleteRole(ctx *fiber.Ctx) error {
	if err := h.isAuthorizedMiddleware(ctx, "roles"); err != nil {
		return ctx.JSON(fiber.Map{
			"permissions": err.Error(),
		})
	}
	//
	id, _ := strconv.Atoi(ctx.Params("id"))

	role, err := h.roleSrvs.DeleteRole(ctx.Context(), id)
	if errors.Is(err, usecase.ErrForeignKey) {
		ctx.Status(fiber.StatusOK)
		return ctx.JSON(fiber.Map{
			"/api/roles/:id": usecase.ErrForeignKey.Error(),
		})
	}
	if err != nil {
		return err
	}

	return ctx.JSON(fiber.Map{
		"/api/roles/:id": role,
	})
}
