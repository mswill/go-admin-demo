package http

import (
	"encoding/hex"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"time"
)

func Upload(ctx *fiber.Ctx) error {

	//-
	form, err := ctx.MultipartForm()
	if err != nil {
		return err
	}

	//-
	files := form.File["image"]
	fileName := ""
	newName := hex.EncodeToString([]byte(fmt.Sprintf("%s_%d", "_", time.Now().Unix())))

	//-
	for _, file := range files {
		fileName = file.Filename
		newName += "_" + fileName
		fmt.Println(fileName)
		if err := ctx.SaveFile(file, "./uploads/"+newName); err != nil {
			return err
		}
	}

	//
	return ctx.JSON(fiber.Map{
		"url": "http://localhost:10000/api/uploads/" + newName,
	})
	//-
	return nil
}
