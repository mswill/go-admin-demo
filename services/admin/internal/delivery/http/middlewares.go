package http

import (
	"context"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/mswill/go-admin-demo/internal/reposiroty/postgres"
	"strconv"
)

func isAuthenticated(ctx *fiber.Ctx) error {
	cookie := ctx.Cookies("jwt")

	//fmt.Println("isAuthenticated: cookies: ", cookie)

	id, err := ParseJWT(cookie)
	if err != nil {
		ctx.Status(fiber.StatusUnauthorized)
		return ctx.JSON(fiber.Map{
			"/api/user": "Unauthorized",
		})
	}
	postgres.GlobalUserID = id
	ctx.SetUserContext(context.WithValue(context.Background(), "userID", id))
	return ctx.Next()
}
func (h *IsAuthorizedHandler) isAuthorizedMiddleware(ctx *fiber.Ctx, page string) error {
	cookie := ctx.Cookies("jwt")
	id, err := ParseJWT(cookie)
	if err != nil {
		fmt.Println(err)
		return err
	}

	//
	uID, _ := strconv.Atoi(id)
	perms, err := h.isAuthorizedSrvs.IsAuthorized(ctx.Context(), uID)
	if err != nil {
		fmt.Println(err)
		return err
	}

	//
	if ctx.Method() == "GET" {
		for _, p := range *perms {
			if p.Name == "view_"+page || p.Name == "edit_"+page {
				return nil
			}
		}
	} else {
		for _, p := range *perms {
			if p.Name == "edit_"+page {
				return nil
			}
		}
	}

	ctx.Status(fiber.StatusUnauthorized)
	return errors.New("Permissions denied")
}
