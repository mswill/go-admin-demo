package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

var _ usecase.IUserService = &UserService{}

func (uc *UserService) GetUsers(ctx context.Context, offset int) (*[]domain.User, int, error) {
	usrs, count, err := uc.userRepo.GetUsers(ctx, offset)
	if err != nil {
		return nil, 0, err
	}

	return usrs, count, nil
}
func (uc *UserService) CreateUser(ctx context.Context, user *domain.User) (string, error) {
	createUser, err := uc.userRepo.CreateUser(ctx, user)
	if err != nil {
		return "", err
	}
	return createUser, nil
}
func (uc *UserService) GetUserByID(ctx context.Context, id int) (*domain.User, error) {
	byID, err := uc.userRepo.GetUserByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return byID, nil
}

func (uc *UserService) UpdateUser(ctx context.Context, user *domain.User, id int) (*domain.User, error) {
	user, err := uc.userRepo.UpdateUser(ctx, user, id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (uc *UserService) DeleteUser(ctx context.Context, id int) (int, error) {
	res, err := uc.userRepo.DeleteUser(ctx, id)
	if err != nil {
		return 0, err
	}
	return res, nil
}
