package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

var _ usecase.IOrderService = &OrderService{}

func (uc *OrderService) CreateOrder(ctx context.Context, order *domain.Order) (string, error) {
	createOrder, err := uc.orderRepo.CreateOrder(ctx, order)
	if err != nil {
		return "", err
	}
	return createOrder, nil
}

func (uc *OrderService) GetOrders(ctx context.Context, offset int) (*[]domain.Order, int, error) {
	orders, countOrders, err := uc.orderRepo.GetOrders(ctx, offset)
	if err != nil {
		return nil, 0, err
	}

	for i, _ := range *orders {
		// set NAME = from first_name and last_name
		(*orders)[i].Name = (*orders)[i].FirstName + " " + (*orders)[i].LastName

		for i2, _ := range (*orders)[i].OrderItems {
			//fmt.Printf("OI PRICE: %0.2f\nOI QUA: %0.2f\n ", (*orders)[i].OrderItems[i2].Price,float32((*orders)[i].OrderItems[i2].Quantity))
			// set TOTAL = from order_lists price and mul with quantity
			(*orders)[i].Total += (*orders)[i].OrderItems[i2].Price * float32((*orders)[i].OrderItems[i2].Quantity)
		}
	}

	return orders, countOrders, nil
}

func (uc *OrderService) GetOrdersCharts(ctx context.Context) (*[]domain.OrderCharts, error) {

	charts, err := uc.orderRepo.GetOrdersCharts(ctx)
	if err != nil {
		return nil, err
	}
	return charts, nil
}
