package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

// check
var _ usecase.IProductService = &ProductsService{}

func (uc *ProductsService) CreateProduct(ctx context.Context, product *domain.Product) (string, error) {
	createProduct, err := uc.prodRepo.CreateProduct(ctx, product)
	if err != nil {
		return "", err
	}

	return createProduct, nil
}
func (uc *ProductsService) GetProducts(ctx context.Context, offset int) (*[]domain.Product, int, error) {
	products, ofs, err := uc.prodRepo.GetProducts(ctx, offset)
	if err != nil {
		return nil, 0, err
	}
	return products, ofs, nil
}
func (uc *ProductsService) GetProductByID(ctx context.Context, id int) (*domain.Product, error) {
	byID, err := uc.prodRepo.GetProductByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return byID, nil
}
func (uc *ProductsService) UpdateProductByID(ctx context.Context, product *domain.Product, id int) (*domain.Product,
	error) {

	updateProduct, err := uc.prodRepo.UpdateProductByID(ctx, product, id)
	if err != nil {
		return nil, err
	}
	return updateProduct, nil
}
func (uc *ProductsService) DeleteProductByID(ctx context.Context, id int) (string, error) {
	product, err := uc.prodRepo.DeleteProductByID(ctx, id)
	if err != nil {
		return "", err
	}
	return product, nil
}
