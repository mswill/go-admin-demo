package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

var _ usecase.IAuthService = &AuthService{}

func (uc *AuthService) UpdateInfo(ctx context.Context, user *domain.User) (*domain.User, error) {

	info, err := uc.authRepo.UpdateInfo(ctx, user)
	if err != nil {
		return nil, err
	}

	return info, nil
}

func (uc *AuthService) UpdatePassword(ctx context.Context, user *domain.User, newPassword string) (string, error) {
	updatedPassword, err := uc.authRepo.UpdatePassword(ctx, user, newPassword)
	if err != nil {
		return "", err
	}
	return updatedPassword, nil
}

func (uc *AuthService) SignUp(ctx context.Context, user *domain.User) (string, error) {
	res, err := uc.authRepo.SignUp(ctx, user)
	if err != nil {
		return "", err
	}
	return res, nil
}

func (uc *AuthService) SignIn(ctx context.Context, user *domain.User) (string, error) {

	token, err := uc.authRepo.SignIn(ctx, user)

	if err != nil {
		return "", err
	}

	return token, nil
}

func (uc *AuthService) GetUser(ctx context.Context, token string) (*domain.User, error) {
	user, err := uc.authRepo.GetUser(ctx, token)
	if err != nil {
		return nil, err
	}
	return user, nil
}
