package logic

import (
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

type AuthService struct {
	authRepo postgres.IAuthRepo
}
type UserService struct {
	userRepo postgres.IUserRepo
}
type RoleService struct {
	roleRepo postgres.IRoleRepo
}
type PermissionService struct {
	perRepo postgres.IPermissionRepo
}
type ProductsService struct {
	prodRepo postgres.IProducts
}
type OrderService struct {
	orderRepo postgres.IOrder
}
type IsAuthorizedMiddlewareService struct {
	isAuthRepo postgres.IsAuthorizedMiddleware
}

type Services struct {
	AuthService
	UserService
	RoleService
	PermissionService
	ProductsService
	OrderService
	IsAuthorizedMiddlewareService
}

func NewServices(repo postgres.IRepositories) *Services {
	return &Services{
		AuthService:                   AuthService{repo},
		UserService:                   UserService{repo},
		RoleService:                   RoleService{roleRepo: repo},
		PermissionService:             PermissionService{perRepo: repo},
		ProductsService:               ProductsService{prodRepo: repo},
		OrderService:                  OrderService{orderRepo: repo},
		IsAuthorizedMiddlewareService: IsAuthorizedMiddlewareService{isAuthRepo: repo},
	}
}
