package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

// check
var _ usecase.IPermissionService = &PermissionService{}

func (uc *PermissionService) AllPermissions(ctx context.Context) (*[]domain.Permissions, error) {
	permissions, err := uc.perRepo.AllPermissions(ctx)
	if err != nil {
		return nil, err
	}
	return permissions, nil
}
