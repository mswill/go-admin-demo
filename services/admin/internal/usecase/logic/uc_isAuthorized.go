package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
)

var _ usecase.IsAuthorizedMiddlewareService = &IsAuthorizedMiddlewareService{}

func (uc *IsAuthorizedMiddlewareService) IsAuthorized(ctx context.Context, userID int) (*[]domain.Permissions, error) {
	authorized, err := uc.isAuthRepo.IsAuthorized(ctx, userID)
	if err != nil {
		return nil, err
	}
	return authorized, nil
}
