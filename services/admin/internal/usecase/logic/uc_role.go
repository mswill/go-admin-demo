package logic

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"strings"
)

var _ usecase.IRoleService = &RoleService{}

func (uc *RoleService) CreateRole(ctx context.Context, role *domain.Role) (*domain.Role, error) {

	role.RoleName = strings.ToLower(role.RoleName)
	createRole, err := uc.roleRepo.CreateRole(ctx, role)
	if err != nil {
		return &domain.Role{}, err
	}
	return createRole, nil
}

func (uc *RoleService) GetRoles(ctx context.Context) (*[]domain.Role, error) {
	roles, err := uc.roleRepo.GetRoles(ctx)
	if err != nil {
		return nil, err
	}
	return roles, err
}

func (uc *RoleService) GetRoleByID(ctx context.Context, id int) (*domain.Role, error) {
	byID, err := uc.roleRepo.GetRoleByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return byID, err
}

func (uc *RoleService) UpdateRole(ctx context.Context, role *domain.Role, id int) (*domain.Role, error) {
	role.RoleName = strings.ToLower(role.RoleName)

	updateRole, err := uc.roleRepo.UpdateRole(ctx, role, id)
	if err != nil {
		return nil, err
	}
	return updateRole, nil
}

func (uc *RoleService) DeleteRole(ctx context.Context, id int) (string, error) {
	role, err := uc.roleRepo.DeleteRole(ctx, id)
	if err != nil {
		return "", err
	}
	return role, err
}
