package usecase

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
)

type IAuthService interface {
	SignUp(ctx context.Context, user *domain.User) (string, error)
	SignIn(ctx context.Context, user *domain.User) (string, error)
	GetUser(ctx context.Context, id string) (*domain.User, error)
	UpdateInfo(ctx context.Context, user *domain.User) (*domain.User, error)
	UpdatePassword(ctx context.Context, user *domain.User, newPassword string) (string, error)
}
type IUserService interface {
	GetUsers(ctx context.Context, offset int) (*[]domain.User, int, error)
	CreateUser(ctx context.Context, user *domain.User) (string, error)
	GetUserByID(ctx context.Context, id int) (*domain.User, error)
	UpdateUser(ctx context.Context, user *domain.User, id int) (*domain.User, error)
	DeleteUser(ctx context.Context, id int) (int, error)
}
type IRoleService interface {
	CreateRole(ctx context.Context, role *domain.Role) (*domain.Role, error)
	GetRoles(ctx context.Context) (*[]domain.Role, error)
	GetRoleByID(ctx context.Context, id int) (*domain.Role, error)
	UpdateRole(ctx context.Context, role *domain.Role, id int) (*domain.Role, error)
	DeleteRole(ctx context.Context, id int) (string, error)
}
type IPermissionService interface {
	AllPermissions(ctx context.Context) (*[]domain.Permissions, error)
}
type IProductService interface {
	GetProducts(ctx context.Context, offset int) (*[]domain.Product, int, error)
	CreateProduct(ctx context.Context, product *domain.Product) (string, error)
	GetProductByID(ctx context.Context, id int) (*domain.Product, error)
	UpdateProductByID(ctx context.Context, product *domain.Product, id int) (*domain.Product, error)
	DeleteProductByID(ctx context.Context, id int) (string, error)
}
type IOrderService interface {
	GetOrders(ctx context.Context, offset int) (*[]domain.Order, int, error)
	CreateOrder(ctx context.Context, order *domain.Order) (string, error)
	GetOrdersCharts(ctx context.Context) (*[]domain.OrderCharts, error)
}
type IsAuthorizedMiddlewareService interface {
	IsAuthorized(ctx context.Context, userID int) (*[]domain.Permissions, error)
}

type IServices interface {
	IAuthService
	IUserService
	IRoleService
	IPermissionService
	IProductService
	IOrderService
	IsAuthorizedMiddlewareService
}
