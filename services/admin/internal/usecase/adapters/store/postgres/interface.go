package postgres

import (
	"context"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
)

type IAuthRepo interface {
	SignUp(ctx context.Context, user *domain.User) (string, error)
	SignIn(ctx context.Context, user *domain.User) (string, error)
	GetUser(ctx context.Context, id string) (*domain.User, error)
	FindUserByEmail(ctx context.Context, user *domain.User) (*domain.User, error)
	CheckLoginUser(ctx context.Context, user *domain.User) (*domain.User, error)
	CreateJWTToken(ctx context.Context, user *domain.User) (string, error)
	UpdateInfo(ctx context.Context, user *domain.User) (*domain.User, error)
	UpdatePassword(ctx context.Context, user *domain.User, newPassword string) (string, error)
}
type IUserRepo interface {
	GetUsers(ctx context.Context, offset int) (*[]domain.User, int, error)
	CreateUser(ctx context.Context, user *domain.User) (string, error)
	GetUserByID(ctx context.Context, id int) (*domain.User, error)
	UpdateUser(ctx context.Context, user *domain.User, id int) (*domain.User, error)
	DeleteUser(ctx context.Context, id int) (int, error)
}
type IRoleRepo interface {
	CreateRole(ctx context.Context, role *domain.Role) (*domain.Role, error)
	GetRoles(ctx context.Context) (*[]domain.Role, error)
	GetRoleByID(ctx context.Context, id int) (*domain.Role, error)
	UpdateRole(ctx context.Context, role *domain.Role, id int) (*domain.Role, error)
	DeleteRole(ctx context.Context, id int) (string, error)
	FindRoleByName(ctx context.Context, name string) (string, bool)
}
type IPermissionRepo interface {
	AllPermissions(ctx context.Context) (*[]domain.Permissions, error)
}
type IProducts interface {
	GetProducts(ctx context.Context, offset int) (*[]domain.Product, int, error)
	CreateProduct(ctx context.Context, product *domain.Product) (string, error)
	GetProductByID(ctx context.Context, id int) (*domain.Product, error)
	UpdateProductByID(ctx context.Context, product *domain.Product, id int) (*domain.Product, error)
	DeleteProductByID(ctx context.Context, id int) (string, error)
}
type IOrder interface {
	GetOrders(ctx context.Context, offset int) (*[]domain.Order, int, error)
	CreateOrder(ctx context.Context, order *domain.Order) (string, error)
	GetOrdersCharts(ctx context.Context) (*[]domain.OrderCharts, error)
	//GetOrderByID(ctx context.Context, id int) (*domain.Order, error)
	//UpdateOrderByID(ctx context.Context, order *domain.Order, id int) (*domain.Order, error)
	//DeleteOrderByID(ctx context.Context, id int) (string, error)
}
type IsAuthorizedMiddleware interface {
	IsAuthorized(ctx context.Context, userID int) (*[]domain.Permissions, error)
}
type IRepositories interface {
	IAuthRepo
	IUserRepo
	IRoleRepo
	IPermissionRepo
	IProducts
	IOrder
	IsAuthorizedMiddleware
}
