package usecase

import "errors"

var (
	ErrEmailExists    = errors.New("this email already exists. Register new email")
	ErrRecordNotFound = errors.New("incorrect data")
	ErrAuthenticated  = errors.New("unauthorized")
	ErrRoleExists     = errors.New("this role already exists")
	ErrForeignKey     = errors.New("this record is related to another record. Can't delete this record")
)
