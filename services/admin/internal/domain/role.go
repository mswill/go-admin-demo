package domain

// Role link between roles и permissions many to many
type Role struct {
	ID          int           `json:"id"`
	RoleName    string        `json:"role_name"`
	Permissions []Permissions `json:"permission" gorm:"many2many:role_permissions"`
}
