package domain

type Permissions struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

/*
insert into permissions (name)
	values
		('view_users'),
	  ('edit_users'),

		('view_roles'),
		('edit_roles'),

	  ('view_products'),
	  ('edit_products'),

		('view_orders'),
	  ('edit_orders');
*/
