package domain

import (
	"crypto/sha1"
	"fmt"
)

// salt
var salt = "sdf"

type User struct {
	ID        uint   `json:"id,omitempty" gorm:"primaryKey;autoIncrement:true"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email" gorm:"unique"`
	Password  string `json:"password"`

	RoleID uint `json:"role_id"`
	Role   Role `json:"role" gorm:"foreignKey:RoleID"`
}

func (u *User) CreatePasswordHash(password string) {
	hash := sha1.New()
	_, err := hash.Write([]byte(password))
	if err != nil {
		fmt.Println("could not to hash")
	}
	u.Password = fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}
