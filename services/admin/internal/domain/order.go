package domain

type Order struct {
	ID         uint        `json:"id" gorm:"primaryKey;autoIncrement:true"`
	FirstName  string      `json:"-"`
	LastName   string      `json:"-"`
	Name       string      `json:"name" gorm:"-"`
	Email      string      `json:"email"`
	Total      float32     `json:"total" gorm:"-"`
	UpdatedAt  string      `json:"updated_at"`
	CreatedAt  string      `json:"created_at"`
	OrderItems []OrderList `json:"order_items" gorm:"foreignKey:OrderID"`
}

type OrderList struct {
	ID           uint    `json:"id" gorm:"primaryKey;autoIncrement:true"`
	OrderID      uint    `json:"order_id"`
	ProductTitle string  `json:"product_title"`
	Price        float32 `json:"price"`
	Quantity     uint    `json:"quantity"`
}
type OrderCharts struct {
	Date string `json:"date"`
	Sum  string `json:"sum"`
}
