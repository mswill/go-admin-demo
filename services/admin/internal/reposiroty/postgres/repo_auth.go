package postgres

import (
	"context"
	"fmt"
	s "github.com/Masterminds/squirrel"
	"strconv"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
	"strings"
	"time"
)

// check
var _ postgres.IRepositories = &Repository{}

// sign jwt token
var MySigningKey = []byte("ssecret")

// token struct
type MyCustomClaims struct {
	ID        int
	FirstName string
	LastName  string
	Email     string
	jwt.RegisteredClaims
}

func (r *Repository) UpdateInfo(ctx context.Context, user *domain.User) (*domain.User, error) {

	var u domain.User

	//-
	sql, i, err := r.authSqlGen.
		Update(userTable).
		Set("first_name", user.FirstName).
		Set("last_name", user.LastName).
		Set("email", user.Email).
		Where(s.Eq{"id": user.ID}).
		Suffix("RETURNING id, first_name, last_name, email").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query for update UpdateInfo")
		return nil, err
	}

	//-
	err = r.authPgx.
		QueryRow(ctx, sql, i...).
		Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
	if err != nil {
		fmt.Println("could not to parse sql for update UpdateInfo")
		return nil, err
	}

	return &u, nil
}
func (r *Repository) UpdatePassword(ctx context.Context, user *domain.User, newPassword string) (string, error) {

	var oldUserPasswordHash domain.User
	var newUserPasswordHash domain.User
	newUserPasswordHash.CreatePasswordHash(newPassword)

	tx, err := r.authPgx.Begin(ctx)
	if err != nil {
		fmt.Println("could not to start TX")
		return "", err
	}

	//-
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("could not to commit")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to rollback")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE!")
	}()

	//-
	sql, i, err := r.authSqlGen.
		Select("id, password").
		From(userTable).
		Where(s.Eq{"id": user.ID}).
		ToSql()
	if err != nil {
		fmt.Println("could not create sql query UpdatePassword")
		return "", err
	}

	//-
	err = tx.QueryRow(ctx, sql, i...).Scan(&oldUserPasswordHash.ID, &oldUserPasswordHash.Password)
	if err != nil {
		fmt.Println("could not to scan sql UpdatePassword")
		return "", err
	}

	//-
	toSql, i2, err := r.authSqlGen.
		Update(userTable).
		Set("password", newUserPasswordHash.Password).
		Where(s.Eq{"password": oldUserPasswordHash.Password}).
		Suffix("RETURNING id, first_name, last_name, password").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query UpdatePassword")
		return "", err
	}

	//-
	var updatedUserPassword domain.User
	err = tx.QueryRow(ctx, toSql, i2...).
		Scan(
			&updatedUserPassword.ID,
			&updatedUserPassword.FirstName,
			&updatedUserPassword.LastName,
			&updatedUserPassword.Password)
	if err != nil {
		fmt.Println("could not to scan")
		return "", err
	}
	fmt.Println("USER: ", updatedUserPassword)

	return "password was updated", nil
}

// done!
func (r *Repository) SignUp(ctx context.Context, user *domain.User) (string, error) {
	// create hash
	user.CreatePasswordHash(user.Password)
	user.Email = strings.ToLower(strings.TrimSpace(user.Email))
	//
	_, err := r.FindUserByEmail(ctx, user)
	if err != nil {
		return "", err
	}
	r.auth.Create(user)

	return "user created", nil
}

// done!
func (r *Repository) SignIn(ctx context.Context, user *domain.User) (string, error) {
	//
	user.Password = strings.TrimSpace(user.Password)
	loginUser, err := r.CheckLoginUser(ctx, user)
	if err != nil {
		return "", err
	}

	token, err := r.CreateJWTToken(ctx, loginUser)
	if err != nil {
		return "", err
	}

	//
	return token, nil
}

// done!
func (r *Repository) GetUser(ctx context.Context, id string) (*domain.User, error) {
	var u domain.User

	parseID, _ := strconv.Atoi(id)
	r.auth.Where("id = ?", parseID).First(&u)
	u.Password = ""
	return &u, nil
}

// done!
func (r *Repository) FindUserByEmail(ctx context.Context, user *domain.User) (*domain.User, error) {
	var u domain.User
	r.auth.Where("email = ?", user.Email).Find(&user).Scan(&u)
	if u.Email == user.Email {
		return &u, usecase.ErrEmailExists
	}

	return &u, nil
}

// done!
func (r *Repository) CheckLoginUser(ctx context.Context, user *domain.User) (*domain.User, error) {
	var u domain.User
	//
	user.CreatePasswordHash(user.Password)

	sql, i, err := r.authSqlGen.
		Select("id, first_name, last_name, email, role_id").
		From(userTable).
		Where(s.Eq{"email": user.Email, "password": user.Password}).
		Limit(1).
		ToSql()
	if err != nil {
		fmt.Println("could not create sql")
		return nil, err
	}

	err = r.authPgx.
		QueryRow(ctx, sql, i...).
		Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email, &u.RoleID)

	//if err == sql2.ErrNoRows {
	//	return nil, usecase.ErrRecordNotFound
	//}
	if err != nil {
		fmt.Println("could not parse sql query ", err)
		return &u, usecase.ErrRecordNotFound
	}

	//
	return &u, nil
}

// JWT token done!
func (r *Repository) CreateJWTToken(ctx context.Context, user *domain.User) (string, error) {
	claims := &MyCustomClaims{
		ID:        int(user.ID),
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,

		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedString, err := token.SignedString(MySigningKey)
	if err != nil {
		return "", err
	}

	return signedString, nil
}
