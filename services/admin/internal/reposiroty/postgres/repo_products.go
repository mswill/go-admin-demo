package postgres

import (
	"context"
	"fmt"
	s "github.com/Masterminds/squirrel"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

var _ postgres.IProducts = &ProductRepository{}
var productsTable = "products"

func (r *ProductRepository) CreateProduct(ctx context.Context, product *domain.Product) (string, error) {

	//-

	//-
	sql, i, err := r.prodSqlGen.
		Insert("").
		Into(productsTable).
		Columns("title, description, image, price").
		Values(product.Title, product.Description, product.Image, product.Price).
		ToSql()

	if err != nil {
		fmt.Println("could not to create sql query product")
		return "", err
	}

	exec, err := r.prodPgx.Exec(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to exec sql products")
		return "", err
	}

	if exec.Insert() {
		return "product was created", nil
	}

	return "something wrong", nil
}
func (r *ProductRepository) GetProducts(ctx context.Context, offset int) (*[]domain.Product, int, error) {

	var prods []domain.Product
	var countProducts int

	tx, err := r.prodPgx.Begin(ctx)
	if err != nil {
		fmt.Println("could not start tx GetProducts")
		return nil, 0, err
	}
	//-
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("could not to commit products GetProducts")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to rollback users GetUsers")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE!")
	}()

	//-
	sql, i, err := r.prodSqlGen.
		Select("id, title, description, image, price").
		From(productsTable).
		OrderBy("id").
		Limit(10).
		Offset(uint64(offset)).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query")
		return nil, 0, err
	}

	//-
	rows, err := tx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to execute sql query ")
		return nil, 0, err
	}

	//-
	for rows.Next() {
		var p domain.Product
		err := rows.Scan(&p.ID, &p.Title, &p.Description, &p.Image, &p.Price)
		if err != nil {
			fmt.Println("could not to scan")
			return nil, 0, err
		}

		prods = append(prods, p)
	}

	//-
	toSql, i2, err := r.prodSqlGen.
		Select("COUNT(*)").
		From(productsTable).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query GetProducts count")
		return nil, 0, err
	}

	//-
	err = tx.QueryRow(ctx, toSql, i2...).Scan(&countProducts)
	if err != nil {
		fmt.Println("could not to scan count products")
		return nil, 0, err
	}

	return &prods, countProducts, nil
}
func (r *ProductRepository) GetProductByID(ctx context.Context, id int) (*domain.Product, error) {

	var prod domain.Product

	//-
	sql, i, err := r.prodSqlGen.
		Select("id, title, description, image, price").
		From(productsTable).
		Where(s.Eq{"id": id}).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql")
		return nil, err
	}

	//-
	err = r.prodPgx.QueryRow(ctx, sql, i...).Scan(&prod.ID, &prod.Title, &prod.Description, &prod.Image, &prod.Price)
	if err != nil {
		fmt.Println("could not to scan")
		return nil, err
	}

	return &prod, nil
}
func (r *ProductRepository) UpdateProductByID(ctx context.Context, product *domain.Product, id int) (*domain.Product, error) {
	var prod domain.Product
	//-
	sql, i, err := r.prodSqlGen.
		Update(productsTable).
		Set("title", product.Title).
		Set("description", product.Description).
		Set("image", product.Image).
		Set("price", product.Price).
		Where(s.Eq{"id": id}).
		Suffix("RETURNING id, title, description, image, price").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query")
		return nil, err
	}

	fmt.Println("I: ", i)
	//-
	err = r.prodPgx.
		QueryRow(ctx, sql, i...).
		Scan(&prod.ID, &prod.Title, &prod.Description, &prod.Image, &prod.Price)
	if err != nil {
		fmt.Println("could not to scan")
		return nil, err
	}

	return &prod, nil
}
func (r *ProductRepository) DeleteProductByID(ctx context.Context, id int) (string, error) {

	//-
	sql, i, err := r.prodSqlGen.
		Delete(productsTable).
		Where(s.Eq{"id": id}).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query")
		return "", err
	}

	//-
	exec, err := r.prodPgx.Exec(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to execute sql")
		return "", err
	}

	if exec.Delete() {
		return "product was deleted", nil
	}

	return "", nil
}
