package postgres

import (
	"context"
	"fmt"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

var _ postgres.IsAuthorizedMiddleware = &IsAuthorizedMiddlewareRepository{}

func (r *IsAuthorizedMiddlewareRepository) IsAuthorized(ctx context.Context, userID int) (*[]domain.Permissions,
	error) {
	var prs []domain.Permissions

	// sql query
	sql := fmt.Sprintf(`SELECT id, name
		FROM permissions 
		WHERE id IN (
			SELECT permissions_id 
			FROM role_permissions 
			WHERE role_id IN (
			  SELECT role_id 
			  FROM users WHERE id = %d))`, userID)

	query, err := r.isAuthPgx.Query(ctx, sql)
	if err != nil {
		fmt.Println("could not to execute sql")
		return nil, err
	}

	for query.Next() {
		var p domain.Permissions
		err := query.Scan(&p.ID, &p.Name)
		if err != nil {
			fmt.Println("could not to scan")
			return nil, err
		}
		prs = append(prs, p)
	}

	return &prs, nil
}
