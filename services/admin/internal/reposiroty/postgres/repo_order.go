package postgres

import (
	"context"
	"fmt"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

const orderTable = "orders"
const orderListsTable = "order_lists"

var GlobalUserID string
var _ postgres.IOrder = &OrderRepository{}

func (r *OrderRepository) CreateOrder(ctx context.Context, order *domain.Order) (string, error) {
	var o domain.Order
	var ol domain.OrderList
	//-
	tx, err := r.orderPgx.Begin(ctx)
	if err != nil {
		fmt.Println("could not to start TX")
		return "", err
	}

	//-
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to ROLLBACK")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE!")
	}()

	//-
	// ORDER
	sql, i, err := r.orderSqlGen.
		Insert("").
		Into(orderTable).
		Columns("first_name", "last_name", "email", "created_at", "updated_at").
		Values(order.FirstName, order.LastName, order.Email, order.CreatedAt, order.UpdatedAt).
		Suffix("RETURNING id, first_name, last_name, email").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query")
		return "", err
	}

	//-
	// insert order
	err = tx.
		QueryRow(ctx, sql, i...).
		Scan(&o.ID, &o.FirstName, &o.LastName, &o.Email)
	if err != nil {
		fmt.Println("could not to scan")
		return "", err
	}

	//-
	// ORDER LIST
	toSql, i2, err := r.orderSqlGen.
		Insert("").
		Into(orderListsTable).
		Columns("order_id", "product_title", "price", "quantity").
		Values(o.ID, "headphones", 600, 1).
		Suffix("RETURNING id, order_id, product_title, price, quantity").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql order_lists")
		return "", err
	}

	//-
	// prepare order
	err = tx.QueryRow(ctx, toSql, i2...).Scan(&ol.ID, &ol.OrderID, &ol.ProductTitle, &ol.Price, &ol.Quantity)
	if err != nil {
		fmt.Println("could not to scan order list")
		return "", err
	}

	//
	fmt.Println("ORDER LISTS: ", ol)
	return "", nil
}
func (r *OrderRepository) GetOrders(ctx context.Context, offset int) (*[]domain.Order, int, error) {

	var oArr []domain.Order
	var orderCount int

	//-
	tx, err := r.orderPgx.Begin(ctx)
	if err != nil {
		fmt.Println("could not to start TX")
		return &oArr, 0, err
	}
	//-
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to ROLLBACK")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE!")
	}()

	//-
	// ORDER SECTION -------------------
	sql, i, err := r.orderSqlGen.
		Select("id, first_name, last_name, email, created_at, updated_at").
		From(orderTable).
		OrderBy("id").
		Limit(5).
		Offset(uint64(offset)).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql orders ")
		return nil, 0, err
	}

	//-
	orderScan, err := tx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to execute sql order")
		return nil, 0, err
	}

	//-
	for orderScan.Next() {
		var o domain.Order
		err := orderScan.Scan(&o.ID, &o.FirstName, &o.LastName, &o.Email, &o.CreatedAt, &o.UpdatedAt)
		if err != nil {
			fmt.Println("could not to scan order")
			return nil, 0, err
		}

		oArr = append(oArr, o)
	}

	//-
	// ORDER LISTS SECTION -------------------
	toSql, i2, err := r.orderSqlGen.
		Select("id, order_id, product_title, price, quantity").
		From(orderListsTable).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql join")
		return nil, 0, err
	}
	//-
	query, err := tx.Query(ctx, toSql, i2...)
	if err != nil {
		fmt.Println("could not to execute join")
		return nil, 0, err
	}
	//-
	for query.Next() {
		var ol domain.OrderList
		err := query.Scan(&ol.ID, &ol.OrderID, &ol.ProductTitle, &ol.Price, &ol.Quantity)
		if err != nil {
			fmt.Println("could not to scan sql join")
			return nil, 0, err
		}

		// adding order_lists to orders
		for i := 0; i < len(oArr); i++ {
			if oArr[i].ID == ol.OrderID {
				oArr[i].OrderItems = append(oArr[i].OrderItems, ol)
			}
		}
	}

	//-
	// ORDERS COUNT  SECTION -------------------
	toSql2, i3, err := r.orderSqlGen.
		Select("COUNT(*)").
		From(orderTable).
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql count")
		return nil, 0, err
	}

	//-
	err = tx.QueryRow(ctx, toSql2, i3...).Scan(&orderCount)
	if err != nil {
		fmt.Println("could not to scan sql count orders")
		return nil, 0, err
	}

	//-

	return &oArr, orderCount, nil
}
func (r *OrderRepository) GetOrdersCharts(ctx context.Context) (*[]domain.OrderCharts, error) {

	var cha []domain.OrderCharts
	sql, i, err := r.orderSqlGen.
		Select("o.created_at as date, sum(ol.price * ol.quantity) as sum").
		From("orders as o").
		InnerJoin("order_lists as ol ON o.id = ol.order_id").
		GroupBy("date").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql ")
		return nil, err
	}

	//-
	row, err := r.orderPgx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to execute sql")
		return nil, err
	}

	for row.Next() {
		var ch domain.OrderCharts
		err := row.Scan(&ch.Date, &ch.Sum)
		if err != nil {
			fmt.Println("could not to scan")
			return nil, err
		}

		cha = append(cha, ch)
	}

	//-

	return &cha, nil
}
