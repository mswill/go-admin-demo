package postgres

import (
	"context"
	"fmt"
	s "github.com/Masterminds/squirrel"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
	"strconv"
	"strings"
)

const roleTable = "roles"
const rolePermissions = "role_permissions"

var _ postgres.IRepositories = &Repository{}

func (r *Repository) CreateRole(ctx context.Context, role *domain.Role) (*domain.Role, error) {
	rRet := &domain.Role{}

	// tx
	tx, err := r.rolePgx.Begin(ctx)
	if err != nil {
		fmt.Println("CreateRole could not start transaction")
		return rRet, err
	}
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("could not commit transaction")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not rollback transaction")
				return
			}
			return
		}
	}()

	//
	// role section
	sql, i, err := r.roleSqlGen.
		Insert(roleTable).
		Columns("role_name").
		Values(role.RoleName).
		Suffix("RETURNING id, role_name").
		ToSql()
	if err != nil {
		fmt.Println("could not create sql string")
		return rRet, err
	}

	//- check role_name
	_, b := r.FindRoleByName(ctx, role.RoleName)
	if b == true {
		return rRet, usecase.ErrRoleExists
	}
	var resRole domain.Role
	err = tx.QueryRow(ctx, sql, i...).Scan(&resRole.ID, &resRole.RoleName)
	if err != nil {
		fmt.Println("could not to scan query")
		return rRet, err
	}
	fmt.Println("ROLE RES: ", resRole)

	//- role_permission section!
	insert := r.perSqlGen.
		Insert("role_permissions").
		Columns("role_id", "permissions_id")
	// create dynamic value
	for _, r := range role.Permissions {
		insert = insert.Values(resRole.ID, r.ID)
	}
	toSql, i2, err := insert.
		Suffix("RETURNING role_id, permissions_id").
		ToSql()
	if err != nil {
		fmt.Println("could not create sql string 2")
		return rRet, err
	}
	fmt.Println("ROLE PERMISSION RES: ", role.Permissions)

	//
	rolPermRes := domain.Permissions{}
	queryPer := tx.QueryRow(ctx, toSql, i2...)
	err = queryPer.Scan(&rolPermRes.ID, &rolPermRes.Name)
	if err != nil {
		fmt.Println("could not scan tx for role_permission")
		return rRet, err
	}

	//  permissions section
	perSql, i3, err := r.perSqlGen.
		Select("roles.id, roles.role_name, permissions.id, permissions.name").
		From("permissions").
		InnerJoin("role_permissions on permissions.id = role_permissions.permissions_id").
		InnerJoin("roles on roles.id = role_permissions.role_id").
		Where(s.Eq{"roles.id": resRole.ID}).
		ToSql()

	//-
	var rls domain.Role
	query, err := tx.Query(ctx, perSql, i3...)
	if err != nil {
		fmt.Println("could not parse tx permission query")
		return rRet, err
	}
	for query.Next() {
		var prs domain.Permissions
		var r domain.Role
		err := query.Scan(&r.ID, &r.RoleName, &prs.ID, &prs.Name)
		if err != nil {
			fmt.Println("could not scan permissions ")
			return rRet, err
		}
		rls.Permissions = append(rls.Permissions, prs)
	}

	//-
	rls.ID = resRole.ID
	rls.RoleName = resRole.RoleName
	fmt.Println("ROLES RES: ", rls)

	//
	return &rls, nil
}

func (r *Repository) GetRoles(ctx context.Context) (*[]domain.Role, error) {

	var rls []domain.Role
	//
	//- tx
	tx, err := r.rolePgx.Begin(ctx)
	if err != nil {
		fmt.Println("GetRoles could not start transaction")
		return &rls, err
	}
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("GetRoles could not commit transaction")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("GetRoles could not rollback transaction")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE! ")
	}()

	//
	//- create sql query for get all roles from db
	sql, i, err := r.roleSqlGen.
		Select("id, role_name").
		From(roleTable).
		OrderBy("id ASC").
		ToSql()
	if err != nil {
		fmt.Println("could not query create")
		return nil, err
	}

	//
	// do sql execute
	query, err := tx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not to parse query")
		return nil, err
	}

	//
	//- add role to roles array -> rls
	for query.Next() {
		var r domain.Role
		err := query.Scan(&r.ID, &r.RoleName)
		if err != nil {
			fmt.Println("could not scan res")
			return nil, err
		}
		rls = append(rls, r)
	}

	//
	//- create sql query. get role.id, role.role_name, permissions.id, permissions.name
	toSql, i2, err := r.perSqlGen.
		Select("r.id, r.role_name, p.id, p.name").
		From("roles as r").
		Join("role_permissions as rp on r.id = rp.role_id ").
		Join("permissions as p on rp.permissions_id = p.id").
		ToSql()
	if err != nil {
		fmt.Println("GetRoles could not create sql ")
		return nil, err
	}

	//
	//- do sql execute
	rows, err := tx.Query(ctx, toSql, i2...)
	if err != nil {
		fmt.Println("could not parse tx permission query")
		return &rls, err
	}

	//-
	// associate permissions with roles. Find all permissions and add them to roles
	for rows.Next() {
		var prs domain.Permissions
		var rr domain.Role
		err := rows.Scan(&rr.ID, &rr.RoleName, &prs.ID, &prs.Name)
		if err != nil {
			fmt.Println("could not scan permissions ")
			return &rls, err
		}

		//
		//- add permissions by role id
		for i, rl := range rls {
			if rl.ID == rr.ID {
				rls[i].ID = rr.ID
				rls[i].RoleName = rr.RoleName
				rls[i].Permissions = append(rl.Permissions, prs)
			}
		}
	}
	//-------------------------
	return &rls, nil
}

func (r *Repository) GetRoleByID(ctx context.Context, id int) (*domain.Role, error) {

	var rl domain.Role
	//
	//- tx
	tx, err := r.rolePgx.Begin(ctx)
	if err != nil {
		fmt.Println("GetRoles could not start transaction")
		return &rl, err
	}
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("GetRoles could not commit transaction")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("GetRoles could not rollback transaction")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE! ")
	}()

	//
	//- create sql query for get all roles from db
	sql, i, err := r.roleSqlGen.
		Select("id, role_name").
		From(roleTable).
		Where(s.Eq{"id": id}).
		ToSql()
	if err != nil {
		fmt.Println("could not query create")
		return nil, err
	}
	//
	// do sql execute
	err = tx.QueryRow(ctx, sql, i...).Scan(&rl.ID, &rl.RoleName)
	if err != nil {
		fmt.Println("could not scan sql query")
		return nil, err
	}

	//
	//- create sql query. get role.id, role.role_name, permissions.id, permissions.name
	toSql, i2, err := r.perSqlGen.
		Select("r.id, r.role_name, p.id, p.name").
		From("roles as r").
		Join("role_permissions as rp on r.id = rp.role_id ").
		Join("permissions as p on rp.permissions_id = p.id").
		ToSql()
	if err != nil {
		fmt.Println("GetRoles could not create sql ")
		return nil, err
	}

	//
	//- do sql execute
	rows, err := tx.Query(ctx, toSql, i2...)
	if err != nil {
		fmt.Println("could not parse tx permission query")
		return &rl, err
	}

	//-
	// associate permissions with roles. Find all permissions and add them to roles
	for rows.Next() {
		var prs domain.Permissions
		var rr domain.Role

		err := rows.Scan(&rr.ID, &rr.RoleName, &prs.ID, &prs.Name)

		if err != nil {
			fmt.Println("could not scan permissions ")
			return &rl, err
		}

		if rl.ID == rr.ID {
			rl.ID = rr.ID
			rl.RoleName = rr.RoleName
			rl.Permissions = append(rl.Permissions, prs)
		}

		//
		//- add permissions by role id
		//for i, rl := range rls {
		//	if rl.ID == rr.ID {
		//		rls[i].ID = rr.ID
		//		rls[i].RoleName = rr.RoleName
		//		rls[i].Permissions = append(rl.Permissions, prs)
		//	}
		//}
	}
	return &rl, nil
}

func (r *Repository) UpdateRole(ctx context.Context, role *domain.Role, id int) (*domain.Role, error) {
	var rr domain.Role

	//-
	tx, err2 := r.rolePgx.Begin(ctx)
	if err2 != nil {
		fmt.Println("could not start transaction")
		return nil, err2
	}
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("could not to commit")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to rollback")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE! ")
	}()

	//-
	sql, i, err := r.roleSqlGen.
		Update(roleTable).
		Set("role_name", role.RoleName).
		Where(s.Eq{"id": id}).
		Suffix("RETURNING *").
		ToSql()
	if err != nil {
		fmt.Println("could not query create")
		return nil, err
	}

	//-
	err = tx.QueryRow(ctx, sql, i...).Scan(&rr.ID, &rr.RoleName)
	if err != nil {
		fmt.Println("could not scan query")
		return nil, err
	}

	//-
	// delete role_permissions
	toSql, i2, err := r.roleSqlGen.
		Delete("role_permissions").
		Where(s.Eq{"role_id": rr.ID}).
		ToSql()
	if err != nil {
		fmt.Println("could not to create query sql for delete")
		return nil, err
	}

	//-
	// do execute sql
	_, err = tx.Exec(ctx, toSql, i2...)
	if err != nil {
		fmt.Println("could not exec delete sql ")
		return nil, err
	}

	//-
	// set to role_permissions
	insert := r.roleSqlGen.
		Insert("").
		Into("role_permissions").
		Columns("role_id, permissions_id")
	for _, rl := range role.Permissions {
		insert = insert.Values(rr.ID, rl.ID)
	}
	toSql2, i3, err := insert.ToSql()
	if err != nil {
		fmt.Println("could not create sql for insert ")
		return nil, err
	}
	fmt.Println("SQL SET : ", toSql2)

	_, err = tx.Exec(ctx, toSql2, i3...)
	if err != nil {
		fmt.Println("could not  execute sql insert")
		return nil, err
	}

	return &rr, nil
}

func (r *Repository) DeleteRole(ctx context.Context, id int) (string, error) {
	tx, err := r.rolePgx.Begin(ctx)
	if err != nil {
		fmt.Println("GetRoles could not start transaction")
		return "", err
	}
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("GetRoles could not commit transaction")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("GetRoles could not rollback transaction")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE! ")
	}()

	// this code delete from role table
	//sql, i, err := r.roleSqlGen.
	//	Delete("*").
	//	From(roleTable).
	//	Where(s.Eq{"id": id}).
	//	ToSql()

	// delete record from table ROLE through deletion from table ROLE_PERMISSIONS
	sql, i, err := r.roleSqlGen.
		Delete("*").
		From(rolePermissions).
		Where(s.Eq{"role_id": id}).
		ToSql()

	if err != nil {
		fmt.Println("could not query delete")
		return "", err
	}

	_, err = tx.Exec(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not parse sql query 1")
		return "", err
	}
	// ---------------
	toSql, i2, err := r.roleSqlGen.
		Delete("*").
		From(roleTable).
		Where(s.Eq{"id": id}).
		ToSql()
	_, err = tx.Exec(ctx, toSql, i2...)
	if err != nil {
		fmt.Println("could not parse sql query 2")
		if strings.Contains(err.Error(), "SQLSTATE 23503") {
			return "", usecase.ErrForeignKey
		}
		return "", err
	}

	return fmt.Sprintf("%s role was deleted", strconv.Itoa(id)), nil
}

// frin
func (r *Repository) FindRoleByName(ctx context.Context, name string) (string, bool) {
	var rol domain.Role

	sql, i, err := r.roleSqlGen.
		Select("role_name").
		From(roleTable).
		Where(s.Eq{"role_name": name}).
		ToSql()

	if err != nil {
		fmt.Println("could not create sql query")
		return "", false
	}
	//
	err = r.rolePgx.QueryRow(ctx, sql, i...).Scan(&rol.RoleName)

	if err != nil {
		fmt.Println("could not to scan from db")
		return "", false
	}

	if rol.RoleName == name {
		return "this role already exists", true
	}

	return "", false
}
