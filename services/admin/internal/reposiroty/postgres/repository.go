package postgres

import (
	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"gorm.io/gorm"
)

type AuthRepository struct {
	auth       *gorm.DB
	authPgx    *pgxpool.Pool
	authSqlGen squirrel.StatementBuilderType
}
type UserRepository struct {
	user       *gorm.DB
	userPgx    *pgxpool.Pool
	userSqlGen squirrel.StatementBuilderType
}
type RoleRepository struct {
	role       *gorm.DB
	rolePgx    *pgxpool.Pool
	roleSqlGen squirrel.StatementBuilderType
}
type PermissionRepository struct {
	per       *gorm.DB
	perPgx    *pgxpool.Pool
	perSqlGen squirrel.StatementBuilderType
}
type ProductRepository struct {
	prod       *gorm.DB
	prodPgx    *pgxpool.Pool
	prodSqlGen squirrel.StatementBuilderType
}
type OrderRepository struct {
	order       *gorm.DB
	orderPgx    *pgxpool.Pool
	orderSqlGen squirrel.StatementBuilderType
}
type IsAuthorizedMiddlewareRepository struct {
	isAuth       *gorm.DB
	isAuthPgx    *pgxpool.Pool
	isAuthSglGen squirrel.StatementBuilderType
}

type Repository struct {
	AuthRepository
	UserRepository
	RoleRepository
	PermissionRepository
	ProductRepository
	OrderRepository
	IsAuthorizedMiddlewareRepository
}

func NewRepositories(db *gorm.DB, pgx *pgxpool.Pool) *Repository {
	return &Repository{
		AuthRepository: AuthRepository{
			auth:       db,
			authPgx:    pgx,
			authSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		},
		UserRepository: UserRepository{
			user:       db,
			userPgx:    pgx,
			userSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)},
		RoleRepository: RoleRepository{
			role:       db,
			rolePgx:    pgx,
			roleSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)},
		PermissionRepository: PermissionRepository{
			per:       db,
			perPgx:    pgx,
			perSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		},
		ProductRepository: ProductRepository{
			prod:       db,
			prodPgx:    pgx,
			prodSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		},
		OrderRepository: OrderRepository{
			order:       db,
			orderPgx:    pgx,
			orderSqlGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		},
		IsAuthorizedMiddlewareRepository: IsAuthorizedMiddlewareRepository{
			isAuth:       db,
			isAuthPgx:    pgx,
			isAuthSglGen: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		},
	}
}
