package postgres

import (
	"context"
	"fmt"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

var perTable = "permissions"
var _ postgres.IPermissionRepo = &PermissionRepository{}

func (r *PermissionRepository) AllPermissions(ctx context.Context) (*[]domain.Permissions, error) {

	var pers []domain.Permissions
	//
	sql, i, err := r.perSqlGen.
		Select("id, name").
		From(perTable).
		ToSql()
	if err != nil {
		fmt.Println("could not create sql query")
		return nil, err
	}

	//
	query, err := r.perPgx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not parse per query")
		return nil, err
	}

	for query.Next() {
		var per domain.Permissions
		err := query.Scan(&per.ID, &per.Name)
		if err != nil {
			fmt.Println("could not scan per")
			return nil, err
		}
		pers = append(pers, per)
	}

	return &pers, nil
}
