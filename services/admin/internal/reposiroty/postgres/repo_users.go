package postgres

import (
	"context"
	"fmt"
	s "github.com/Masterminds/squirrel"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gitlab.com/mswill/go-admin-demo/internal/usecase/adapters/store/postgres"
)

// check
var _ postgres.IRepositories = &Repository{}
var userTable = "users"

func (r *Repository) GetUsers(ctx context.Context, offset int) (*[]domain.User, int, error) {
	var usrs []domain.User
	var countUsers int
	//
	tx, err := r.userPgx.Begin(ctx)
	if err != nil {
		fmt.Println("could not start tx GetUsers")
		return nil, 0, err
	}

	//-
	defer func() {
		err := tx.Commit(ctx)
		if err != nil {
			fmt.Println("could not to commit users GetUsers")
			err := tx.Rollback(ctx)
			if err != nil {
				fmt.Println("could not to rollback users GetUsers")
				return
			}
			return
		}
		fmt.Println("COMMIT IS DONE!")
	}()

	//
	sql, i, err := r.userSqlGen.
		Select("users.id, first_name, last_name, email, roles.id, roles.id, roles.role_name").
		From(userTable).
		Join("roles on roles.id = users.role_id").
		OrderBy("users.id").
		Limit(10).
		Offset(uint64(offset)).
		ToSql()
	if err != nil {
		fmt.Println("could not create sql query")
		return nil, 0, err
	}
	//
	query, err := tx.Query(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not sql parse")
		return nil, 0, err
	}

	//
	for query.Next() {
		var u domain.User
		err := query.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email, &u.RoleID, &u.Role.ID, &u.Role.RoleName)
		if err != nil {
			fmt.Println("could not scan sql")
			return nil, 0, err
		}
		usrs = append(usrs, u)
	}

	toSql, i2, err := r.userSqlGen.
		Select("COUNT(*)").
		From(userTable).
		Join("roles on roles.id = users.role_id").
		ToSql()
	if err != nil {
		fmt.Println("could not to create sql query GetUsers count")
		return nil, 0, err
	}

	//
	err = r.userPgx.
		QueryRow(ctx, toSql, i2...).
		Scan(&countUsers)
	if err != nil {
		fmt.Println("could not to scan countUsers GetUsers")
		return nil, 0, err
	}

	return &usrs, countUsers, nil
}
func (r *Repository) CreateUser(ctx context.Context, user *domain.User) (string, error) {
	user.CreatePasswordHash("qwerty")

	loginUser, err := r.FindUserByEmail(ctx, user)
	if err != nil {
		return "", err
	}
	fmt.Println("::: ", loginUser)
	r.user.Create(user)

	return "new user was created", nil
}
func (r *Repository) GetUserByID(ctx context.Context, id int) (*domain.User, error) {
	var u domain.User

	sql, i, err := r.userSqlGen.
		Select("users.id, first_name, last_name, email, roles.id, roles.id, roles.role_name").
		From(userTable).
		Join("roles on roles.id = users.role_id").
		Where(s.Eq{"users.id": id}).
		ToSql()

	if err != nil {
		fmt.Println("could not create sql query")
		return nil, err
	}
	//
	row := r.userPgx.QueryRow(ctx, sql, i...)
	if err != nil {
		fmt.Println("could not sql parse")
		return nil, err
	}
	//

	row.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email, &u.RoleID, &u.Role.ID, &u.Role.RoleName)

	return &u, nil
}
func (r *Repository) UpdateUser(ctx context.Context, user *domain.User, id int) (*domain.User, error) {
	var uReturning domain.User
	sql, args, err := r.userSqlGen.Update(userTable).
		Set("first_name", user.FirstName).
		Set("last_name", user.LastName).
		Set("email", user.Email).
		Set("role_id", user.RoleID).
		Where(s.Eq{"id": id}).
		Suffix("RETURNING *").
		ToSql()

	if err != nil {
		return nil, err
	}
	//

	err = r.userPgx.
		QueryRow(ctx, sql, args...).
		Scan(
			&uReturning.ID,
			&uReturning.FirstName,
			&uReturning.LastName,
			&uReturning.Email,
			&uReturning.Password,
			&uReturning.RoleID)
	if err != nil {
		return nil, err
	}

	uReturning.Password = ""
	return &uReturning, nil
}
func (r *Repository) DeleteUser(ctx context.Context, id int) (int, error) {

	sql, args, err := r.userSqlGen.
		Delete(userTable).
		Where(s.Eq{"id": id}).
		Suffix("RETURNING id").
		ToSql()
	//
	if err != nil {
		return 0, err
	}
	//
	_, err = r.userPgx.Exec(ctx, sql, args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}
