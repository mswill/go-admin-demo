package DTO

type RoleCreateDTO struct {
	RoleName    string   `json:"role_name"`
	Permissions []string `json:"permissions"`
}
