package DTO

type UpdateUserInfo struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type UpdateUserPassword struct {
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
}
