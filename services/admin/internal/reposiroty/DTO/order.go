package DTO

import "gitlab.com/mswill/go-admin-demo/internal/domain"

type OrderDTO struct {
	Order     domain.Order
	OrderList domain.OrderList
}
