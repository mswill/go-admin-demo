package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mswill/go-admin-demo/internal/domain"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

var PG_DSN = os.Getenv("PG_DSN")

func ConnectBD() *gorm.DB {
	db, err := gorm.Open(postgres.Open(PG_DSN), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	// migrate

	err = db.AutoMigrate(
		&domain.User{},
		&domain.Role{},
		&domain.Permissions{},
		&domain.Product{},
		&domain.Order{},
		&domain.OrderList{},
	)
	if err != nil {
		log.Fatal(err)
	}
	log.Infof("Migrate is done!")
	return db
}

// pgx connect
func ConnectPGX() *pgxpool.Pool {
	pool, err := pgxpool.New(context.Background(), PG_DSN)
	if err != nil {
		panic(err)
	}

	err = pool.Ping(context.Background())
	if err != nil {
		log.Fatal("could not to ping! ", err.Error())
	}
	fmt.Println("Pink it's ok! ")
	// return
	return pool
}
