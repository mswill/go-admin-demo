package server

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/spf13/viper"

	"log"
	"os"
	"strings"
	"time"
)

// GetServerInstance get instance
func GetServerInstance() *fiber.App {

	app := fiber.New(fiber.Config{
		Prefork:      false,
		AppName:      "go-admin-demo",
		BodyLimit:    1024 * 1024 * 5,
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 5,
	})

	app.Use(compress.New())
	app.Use(cors.New(cors.Config{AllowCredentials: true}))
	app.Use(logger.New())
	app.Use(recover.New())

	return app
}

// Run server run
func Run(app *fiber.App) {

	port := viper.Get("SERVER_PORT")

	fmt.Printf("%s", strings.Repeat("-", 40))
	fmt.Println("\n")
	fmt.Printf("%s %s", "SERVER_PORT RUN ON PORT ", port)
	fmt.Println("\n")
	fmt.Printf("%s\n", strings.Repeat("-", 40))

	if fiber.IsChild() {
		fmt.Printf("\n[%d] Child\n", os.Getppid())
	} else {
		fmt.Printf("\n[%d] Master\n", os.Getppid())
	}

	err := app.Listen(":" + port.(string))
	if err != nil {
		panic(err)
	}
}

// ShutDown graceful
func ShutDown(app *fiber.App) {
	fmt.Println("SERVER STOPPING THROW 2 SEC ")
	t := time.Tick(time.Second * 2)
	select {
	case <-t:
		err := app.Shutdown()
		if err != nil {
			log.Fatal("could not server stopping ", err)
		}
	}

}
