import { axiosIns } from "@/plugins/axios";
import type {
	LoginUserType,
	UrlType,
	RegUserType,
	UserCreateType,
	UsersType,
	RoleType,
	RoleForUpdate,
	ProductsType
} from '@/types'
import type { ProfileInfoType, ProfilePasswordType } from "@/types/profile";


// USER
export const Register = async ({ url }: UrlType, userForReg: RegUserType): Promise<string> => {
	const { data } = await axiosIns.post(url, userForReg);
	return data
}
export const Login = async ({ url }: UrlType, userForLogin: LoginUserType): Promise<any> => {
	return axiosIns.post(url, userForLogin)
}
export const Logout = async ({ url }: UrlType): Promise<any> => {
	return axiosIns.post(url)
}
export const GetUser = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const DeleteUserByID = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.delete(url)
}
export const GetUsers = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const UserCreate = async ({ url }: UrlType, user: UserCreateType): Promise<any> => {
	console.log(user);
	return await axiosIns.post(url, user)
}
export const GetUserByID = async ({ url }: UrlType, id: string): Promise<any> => {
	return await axiosIns.get(url, { params: { 'id': id } })
}
export const UpdateUserByID = async ({ url }: UrlType, user: UsersType): Promise<any> => {
	return await axiosIns.put(url, user)
}

// ROLES
export const GetRoles = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const GetRoleByID = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const DeleteRole = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.delete(url)
}
export const CreateRole = async ({ url }: UrlType, role: any): Promise<any> => {
	return await axiosIns.post(url, role);
}
export const UpdateRole = async ({ url }: UrlType, role: RoleForUpdate): Promise<any> => {
	return await axiosIns.put(url, role)
};

// PERMISSIONS
export const GetPermissions = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}

// PRODUCTS
export const GetProducts = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const GetProductByID = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.get(url)
}
export const DeleteProductByID = async ({ url }: UrlType): Promise<any> => {
	return await axiosIns.delete(url)
}
export const ImageUpload = async ({ url }: UrlType, image: FormData): Promise<any> => {
	return await axiosIns.post(url, image)
}
export const CreateProduct = async ({ url }: UrlType, product: ProductsType): Promise<any> => {
	return await axiosIns.post(url, product)
}
export const UpdateProduct = async ({ url }: UrlType, product: ProductsType): Promise<any> => {
	return await axiosIns.put(url, product)
}

// ORDERS
export const GetOrders = async ({ url }: UrlType): Promise<any> => {
	console.log(url);
	return await axiosIns.get(url)
};
export const GetOrdersCharts = async ({ url }: UrlType): Promise<any> => {
return await axiosIns.get(url)
}


// UPDATE PROFILE
export const UpdateInfo = async ({url}: UrlType, user: ProfileInfoType):Promise<any>=> {
	return await axiosIns.post(url, user)
}
export const UpdatePassword = async ({url}: UrlType, pass: ProfilePasswordType):Promise<any>=> {
	return await axiosIns.post(url, pass)
}
