export interface RegUserType {
	first_name: string
	last_name: string
	email: string
	password: string
}

export interface LoginUserType {
	email: string
	password: string
}

export interface UrlType {
	url: string
}

export interface PermissionsType {
	id: number
	name: string
}

export interface RoleType {
	id: number
	role_name: string
	permission: Array<PermissionsType>
}

export interface RoleForUpdate {
	role_name: string
	permissions: Array<string>
}

export interface UsersType {
	id: number
	first_name: string
	last_name: string
	email: string
	role_id: number | string
}

export interface Meta {
	page: number,
	last_page: number,
	total: number
}

export interface UserCreateType {
	first_name: string
	last_name: string
	email: string
	role_id: number | string
}

export interface ProductsType  {
	id: number
	title: string
	description: string
	price:  number | string
	image: any
}
