import { srCyrl } from "vuetify/locale";

export interface OrderItemsType {
	id: number,
	order_id: number,
	product_title: string,
	price: number,
	quantity: number
}

export interface OrdersType  {
	id: number,
	name: string,
	email: string,
	total: number,
	updated_at: string,
	created_at: string,
	orderItems: Array<OrderItemsType>
}
export interface OrdersChartsType {
	date: string,
	sum: string
}
