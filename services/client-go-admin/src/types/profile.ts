export interface ProfileInfoType {
	id: number
	first_name: string,
	last_name: string
	email: string
}

export interface ProfilePasswordType {
	password: string,
	confirm_password: string
}
