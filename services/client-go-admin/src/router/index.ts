import { createRouter, createWebHistory } from 'vue-router'
import type {RouteRecordRaw} from 'vue-router'

import Wrapper from '../views/Wrapper.vue'
import Dashboard from '../views/Dashboard.vue'
import Profile from '../views/Profile.vue'
// users
import Registration from '../views/Registration.vue'
import Login from '../views/Login.vue'
import TableUsers from '../components/users/TableUsers.vue'
import UsersCreate from '../components/users/CreateUser.vue'
import EditUser from '../components/users/EditUser.vue'
// roles
import Roles from '../components/roles/Roles.vue'
import EditRoles from '../components/roles/EditRoles.vue'
import CreateRole from '../components/roles/CreateRole.vue'

// products
import Products from '../components/products/Products.vue'
import Product from '../components/products/Product.vue'
import CreateProduct from '../components/products/CreateProduct.vue'


// orders
import Orders from '../components/orders/Orders.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path:'/register',
    name: 'Registration',
    component: Registration
  },
  {
    path:'/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Wrapper',
    component: Wrapper,
    children: [
      {
        path: 'users',
        name: 'Users',
        component: TableUsers
      },
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: 'users/create',
        name: 'UsersCreate',
        component: UsersCreate
      },
      {
        path: 'user/:id/edit',
        name: 'EditUser',
        component: EditUser
      },
      {
        path: 'roles',
        name: 'Roles',
        component: Roles
      },
      {
        path: 'role/:id/edit',
        name: 'EditRole',
        component: EditRoles
      },
      {
        path: 'roles/create',
        name: 'RolesCreate',
        component: CreateRole
      },
      {
        path: 'products',
        name: 'Products',
        component: Products
      },
      {
        path: 'product/:id/edit',
        name: 'Product',
        component: Product
      },
      {
        path: 'product/create',
        name: 'CreateProduct',
        component: CreateProduct
      },
      {
        path: 'orders',
        name: 'Orders',
        component: Orders
      },
      {
        path: 'profile',
        name: 'Profile',
        component: Profile
      }
    ]
  },


  {
    path: '/:path(.*)*',
    name: 'notFound',
    redirect: '/'
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
