import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// charts
import { Chart as ChartJS, Title, Tooltip, Legend, BarElement, CategoryScale, LinearScale } from 'chart.js'
ChartJS.register(Title, Tooltip, Legend, BarElement, CategoryScale, LinearScale)


const app = createApp(App)
// toast
import Toast from './plugins/toast'
// vuetify
import { vuetify } from './plugins/vuetify'
app.use(Toast,{
	transition: "Vue-Toastification__fade",
	maxToasts: 3,
	newestOnTop: true
})
app.use(vuetify)
app.use(createPinia())
app.use(router)

app.mount('#app')
