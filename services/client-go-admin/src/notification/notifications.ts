import { useToast } from "vue-toastification";

const toast = useToast()

const options: Object = {
	position: "top-right",
	timeout: 2000,
	closeOnClick: false,
	pauseOnFocusLoss: false,
	pauseOnHover: false,
	draggable: false,
	draggablePercent: 0.34,
	showCloseButtonOnHover: false,
	hideProgressBar: true,
	closeButton: "button",
	icon: "fas fa-circle-xmark",
	rtl: false
}

export const Error = (text: string, typeToast: string): void => {
	// @ts-ignore
	toast[typeToast](text, {
		...options,
	})
};

export const Success = (text: string, typeToast: string, icon: string): void => {
	// @ts-ignore
	toast[typeToast](text,
		{
			...options,
			icon: icon,
		})
}
export const Warning = (text: string, typeToast: string, icon: string): void => {
	// @ts-ignore
	toast[typeToast](text,
		{
			...options,
			icon: icon,
		})
}
export const Info = (text: string, typeToast: string, icon: string): void => {
	// @ts-ignore
	toast[typeToast](text,
		{
			...options,
			icon: icon,
		})
}
