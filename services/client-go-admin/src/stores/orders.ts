import { defineStore } from 'pinia'
import { GetOrders, GetOrdersCharts } from "@/api";
import type { OrdersChartsType, OrdersType } from '@/types/orders';
import type { Meta } from "@/types";
import _ from 'lodash'
export const useOrdersStore = defineStore({
	id: 'orders',
	state: () => ({
			orders: [] as Array<OrdersType>,
			ordersMeta: {} as Meta,
			ordersCharts: []  as Array<OrdersChartsType>,
			labels: [] as Array<string>,
			data: [] as Array<string>
	}),
	actions: {
		async SendGetOrders(pageID: string) {
			const { data } = await GetOrders({ url: `/orders?page=${ pageID }` });
			this.orders = data['/api/orders']
			this.ordersMeta = data['/api/orders/meta']
			console.log(data);
		},
		async SendGetOrderCharts(){
			try {
				const { data } = await GetOrdersCharts({ url: '/orders/charts' });
				this.ordersCharts =  data['api/order/charts']
				this.labels =  _.map(this.ordersCharts, item => item.date)
				this.data = _.map(this.ordersCharts, item => item.sum)

			}catch (e:any) {
				console.log(e.response.data);
			}
		},
	},
	getters: {
		getOrders: state => state.orders,
		getOrdersMeta: state => state.ordersMeta,
		getOrdersCharts: state => state.ordersCharts,
		getLabels: state => state.labels,
		getData: state => state.data,
	}
})
