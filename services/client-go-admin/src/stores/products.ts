import { defineStore } from 'pinia'
import { CreateProduct, DeleteProductByID, GetProductByID, GetProducts, ImageUpload, UpdateProduct } from "@/api";
import type { Meta, ProductsType } from '@/types'
import _ from 'lodash';


export const useProductStore = defineStore({
	id: 'product',
	state: () => ({
		products: [] as Array<ProductsType>,
		product: {} as ProductsType,
		metaProducts: {} as Meta,
		productMsg: '',
		productMsgDen: ''
	}),
	actions: {
		async SendGetProductByID(prodID: string) {
			try {
				const { data } = await GetProductByID({ url: `/products/${ prodID }` })
				console.log(data);
				this.product = data['/api/products/:id']
			} catch (e: any) {
				console.log(e.response.data);
			}
		},
		async SendUpdateProduct(imageFile: FormData | undefined, prodID: number, product: ProductsType) {
			try {
				const { data } = await ImageUpload({ url: '/upload' }, imageFile!)
				if (data['url'].includes('http')) {
					product.image = data['url']
					product.price = Number(product.price)
					const res = await UpdateProduct({ url: `/products/${prodID}` }, product);
					console.log(res.data);
				}
			} catch (e: any) {
				console.log(e.response.data);
				this.productMsgDen = e.response.data
			}
		},
		async SendCreateProduct(imageFile: FormData | undefined, product: ProductsType) {
			try {
				const { data } = await ImageUpload({ url: '/upload' }, imageFile!)
				if (data['url'].includes('http')) {
					product.image = data['url']
					product.price = Number(product.price)
					const res = await CreateProduct({ url: '/products' }, product);
					console.log(res.data);
				}
			} catch (e: any) {
				console.log(e.response.data);
				this.productMsgDen = e.response.data
			}
		},
		async SendDeleteProductByID(prodID: number): Promise<any> {
			try {
				const { data } = await DeleteProductByID({ url: `/products/${ prodID }` });
				this.productMsg = data['/api/product/:id']
				this.products = _.filter(this.products, p => p.id !== prodID);
			}catch (e:any) {
				console.log(e.response.data);
				this.productMsgDen = e.response.data
			}
		},
		async SendGetProducts(val: number): Promise<any> {
			try {
				const { data } = await GetProducts({ url: `/products?page=${ val }` });
				this.products = data['/api/products']
				this.metaProducts = data['/api/products/meta']
				return this.metaProducts
			} catch (e: any) {
				console.log(e.response.data);
			}
		},
	},
	getters: {
		getProducts: state => state.products,
		getProduct: state => state.product,
		getMetaProducts: state => state.metaProducts,
		getProductMsg: state => state.productMsg,
		getProductMsgDen: state => state.productMsgDen
	}
})
