import { defineStore } from 'pinia'
import type { ProfileInfoType, ProfilePasswordType } from '@/types/profile'
import { UpdateInfo, UpdatePassword } from '@/api'

export const useProfileStore = defineStore({
	id: 'profile',
	state: () => ({}),
	actions: {
		async SendChangeProfileInfo(u: ProfileInfoType) {
			try {
				const { data } = await UpdateInfo({ url: '/users/info' }, u)
				console.log(data);
			} catch (e: any) {
				console.log(e.response.data);
			}
		},
		async SendChangeProfilePassword(p: ProfilePasswordType) {
			try {
				const { data } = await UpdatePassword({ url: '/users/password' }, p);
				console.log(data);
			} catch (e: any) {
				console.log(e.response.data);
			}
		},
	},
	getters: {}
})
