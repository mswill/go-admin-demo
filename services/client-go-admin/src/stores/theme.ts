import { defineStore } from 'pinia'

export const useThemeStore = defineStore({
	id: 'theme',
	state: () => ({
		themeColor: '',
		changeColorTheme: 'light'
	}),
	actions: {
		async setChangeColorTheme(val: string):Promise<void> {
			this.changeColorTheme = val
			await this.setColorThemeLocalStore(val)
		},
		async setColorThemeLocalStore(val: string) {
				localStorage.setItem('themeColor', val)
		},
		async getColorThemeLocalStore() {
			const color = localStorage.getItem('themeColor');
			if (color === '') {
				this.themeColor = 'light'
			}
			console.log('STORE: ', color);
			this.themeColor = color!
			this.changeColorTheme = color!
		}
	},
	getters: {
		getThemeColor: state => state.themeColor,
		getChangeColorTheme: state => state.changeColorTheme
	}
})
