import { defineStore } from 'pinia'

// @ts-ignore
import _ from 'lodash'
import {
	Login,
	Register,
	GetUser,
	GetUsers,
	Logout,
	DeleteUserByID,
	UserCreate,
	GetUserByID,
	UpdateUserByID
} from '@/api/'
import type { LoginUserType, RegUserType, UserCreateType, UsersType } from '@/types'


export const useUserStore = defineStore({
	id: 'user',
	state: () => ({
		users: [] as [] | any,
		metaUsers: [],
		user: {},
		regIsOk: '',
		token: '',
		userMsg: '',
		unauthorized: false
	}),
	actions: {
		async SendUpdateUpdateUserByID(user: UsersType, id: string):Promise<void> {
			try {
				const res = await UpdateUserByID({ url: `/users/${ id }` }, user)
			} catch (e: any) {
				this.userMsg = e.response.data
				console.log('catch', e.response.data);
			}
		},
		async SendGetUserByID(userID: string):Promise<any> {
			try {
				const { data } = await GetUserByID({ url: `/users/${ userID }` }, userID);
				return data['/api/user/:id']
			} catch (e: any) {
				console.log(e.response.data);
			}
		},
		async SendRegister(user: RegUserType): Promise<string> {
			try {
				this.regIsOk = await Register({ url: '/register' }, user);
				// @ts-ignore
				return this.regIsOk['msg']
			} catch (e) {
				throw e
			}
		},
		async SendLogin(user: LoginUserType): Promise<any> {
			try {
				const { data } = await Login({ url: '/login' }, user);
				this.token = await data;
				return this.token
			} catch (e: any) {
				console.log(e.response.data);
				this.userMsg = e.response.data
				return e.response.data
			}
		},
		async SendGetUser(): Promise<void> {
			try {
				const { data } = await GetUser({ url: '/user' });
				this.user = await data['/api/getUser']
				this.unauthorized = false
			} catch (e: any) {
				if (e.response.data['/api/user'] === 'Unauthorized') {
					this.unauthorized = true
				}
			}
		},
		async SendGetUsers(val: number): Promise<any> {
			try {
				const { data } = await GetUsers({ url: `/users?page=${ val }` });
				const meta = await data['/api/users/meta']
				const users = await data['/api/users']
				this.users = await users
				this.metaUsers = await meta
				this.unauthorized = false
				return { 'us': this.users, 'meta': this.metaUsers };
			} catch (e: any) {
				if (e.response.data['/api/user'] === 'Unauthorized') {
					this.unauthorized = true
				}
			}
		},
		async SendDeleteUserByID(id: number): Promise<string | undefined> {
			try {
				const { data } = await DeleteUserByID({ url: `/users/${ id }` });
				this.users = _.filter(this.users, (u: any) => u.id !== data['/api/users'])
				return data['/api/users']
			} catch (e: any) {
				console.log(e.response.data);
				this.userMsg = e.response.data
			}
		},
		async SendLogout(): Promise<any> {
			await Logout({ url: '/logout' })
		},
		async SendUserCreate(user: UserCreateType): Promise<string | undefined> {
			try {
				user.role_id = Number(user.role_id)
				const { data } = await UserCreate({ url: '/users' }, user)
				return data['/api/users']
			} catch (e: any) {
				this.userMsg = e.response.data
			}
		},
		// local store
		async SetToLocalStoreUser(): Promise<void> {
			localStorage.setItem('user', JSON.stringify(this.user))
		},
		async GetFromLocalStoreUser(): Promise<void> {
			const u = await localStorage.getItem('user');
			if (u != null || u != undefined) {
				this.user = await JSON.parse(u);
			}
		},
		// clear all for logout
		async Logout(): Promise<void> {
			this.user = {}
			this.token = ''
			this.userMsg = ''
			this.unauthorized = true
			// logout backend
			await this.SendLogout()
		}
	},

	getters: {
		getRegIsOK: state => state.regIsOk,
		getuserMsg: state => state.userMsg,
		getCurrentUser: state => state.user,
		getUsers: state => state.users
	}
})
