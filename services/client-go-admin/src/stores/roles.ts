import { defineStore } from "pinia";
import { CreateRole, DeleteRole, GetRoleByID, GetRoles, UpdateRole } from '@/api'
import _ from 'lodash'
import type { RoleType, RoleForUpdate } from '@/types'

export const useRolesStore = defineStore(
	'roles',
	{
		state: () => ({
			roles: [] as Array<RoleType>,
			role: {} as RoleType,
			roleMsg: ''
		}),
		actions: {
			async SendGetRoleByID(roleID: string) {
				try {
					const { data } = await GetRoleByID({ url: `roles/${ roleID }` });
					console.log(data);
					this.role = data['/api/role/:id']
				} catch (e: any) {
					console.log(e.response.data);
				}
			},
			async SendUpdateRole(roleID: number, role: RoleForUpdate): Promise<any> {
				try {
					console.log(roleID);
					console.log(role);
					const { data } = await UpdateRole({ url: `/roles/${ roleID }` }, role)
					console.log('role update: ', data);
				} catch (e: any) {
					console.log(e.response.data);
					this.roleMsg =  e.response.data
				}
			},
			async SendCreateRole(role: any): Promise<void> {
				try {
					const { data } = await CreateRole({ url: '/roles' }, role)
				} catch (e: any) {
					console.log('SendCreateRole ', e.response.data);
					this.roleMsg =  e.response.data
				}
			},
			async SendDeleteRoleByID(roleID: number): Promise<string | undefined> {
				try {
					const { data } = await DeleteRole({ url: `/roles/${ roleID }` })
					//- if err exists
					if (data['/api/roles/:id'].includes("related to another record")) return data['/api/roles/:id']
					//- if not exists
					this.roles = _.filter(this.roles, r => r.id !== roleID);
					return data['/api/roles/:id']
				} catch (e: any) {
					console.log('catch ', e.response.data);
					this.roleMsg =  e.response.data
				}
			},
			async SendGetRoles(): Promise<any> {
				try {
					const { data } = await GetRoles({ url: '/roles' });
					this.roles = await data['/api/roles']
				} catch (e: any) {
					console.log('SendGetRoles ', e.response.data);
				}
			},
		},
		getters: {
			getRoles: state => state.roles,
			getRole: state => state.role,
			getRoleMsg: state => state.roleMsg
		}
	})
