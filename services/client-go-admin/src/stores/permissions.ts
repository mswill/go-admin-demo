import { defineStore } from "pinia";
import _ from 'lodash'
import type { PermissionsType } from '@/types'
import { CreateRole, GetPermissions } from "@/api";
import { da } from "vuetify/locale";

export const usePermissionsStore = defineStore(
	'permissions',
	{
		state: () => ({
			permissions: [] as Array<PermissionsType>,
		}),

		actions: {
			async SendGetPermissions(): Promise<any> {
				try {
					const { data } = await GetPermissions({ url: '/permissions' })
					this.permissions = await data['/api/permissions']
				} catch (e: any) {
					console.log('SendGetPermissions ', e.response.data);
				}
			},
		},
		getters: {
			getPerms: state => state.permissions
		}
	})
