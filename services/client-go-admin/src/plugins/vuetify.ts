'use strict';

// Vuetify
import 'vuetify/styles'

// styles
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/css/fontawesome.css'
import '@fortawesome/fontawesome-free/css/brands.min.css'
import '@fortawesome/fontawesome-free/css/regular.css'
import { aliases, fa } from 'vuetify/iconsets/fa'

import { createVuetify } from 'vuetify'
import type { ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const myCustomTheme: ThemeDefinition = {
	dark: false,
	colors: {
		background: '#191D32',
		surface: '#282F44',
		primary: '#064663',
		'primary-darken-1': '#043145',
		secondary: '#ECB365',
		'secondary-darken-1': '#b98c4d',
		error: '#B00020',
		info: '#2196F3',
		success: '#4CAF50',
		warning: '#FB8C00',
	}
}


export const vuetify = createVuetify({
	components,
	directives,
	icons: {
		defaultSet: 'fa',
		aliases,
		sets: {
			fa,
		},
	},
	theme: {
		defaultTheme: 'myCustomTheme',
		themes: {
			myCustomTheme,
		}
	}
})
